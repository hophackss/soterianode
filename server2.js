var config = require("./config.js");

var mongo = require('mongoskin');

//var mongodb
//var connection = mongo.createConnection(config.mysql);

var express = require("express");


var bodyParser  = require("body-parser");
var md5 = require('MD5');
var rest = require("./rest2.js");
var app  = express();

global_log ='';

global_oldInfo =new Array();

global_deviceStatus = new Array();


function REST(){
    var self = this;
    self.connectMongoDB();
};


REST.prototype.connectMongoDB = function() {
    var self = this;
   // var pool      =    mysql.createPool(config.mysqlPool);
   // pool.getConnection(function(err,connection){
   //     if(err) {
   //       self.stop(err);
   //     } else {
          self.configureExpress();
   //     }
   // });
}

REST.prototype.configureExpress = function(connection) {
      var self = this;
      app.use(bodyParser.urlencoded({ extended: true }));
      app.use(bodyParser.json());
      var router = express.Router();

      app.use(function(req, res, next) {
		  res.header("Access-Control-Allow-Origin", "*");
  		  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
 	 	  next();
		});

      app.use('/', router);

      var rest_router = new rest(router,connection,md5);
      self.startServer();
}

REST.prototype.startServer = function() {
      app.listen(config.port,function(){
          console.log("All right ! I am alive at Port"+config.port);
      });
}

REST.prototype.stop = function(err) {
    console.log("ISSUE WITH MongoDB \n" + err);
    process.exit(1);
}



sample_json = [{
    "deviceId" : "123423423", 
    "coords":{
      "latitude" : "39.3299013",
      "longitude": "-76.6205177"          
    },
    "userInfo": {
        "userType": "student",
        "name": "Piyush Madan",
        "contact": "+1-980-777-6044",
        "emergency": "+1-980-777-0434",
        "age": 25,
        "gender":"M"
        },
    "destination": {
      "coords":{
              "latitude" : "39.3372681",
            "longitude": "-76.6243973"
          },
            "type": "home"
          },
    "timestamp": "2016-02-06T00:20:19Z" ,
    "emergency": true,
    "enroute": true
  },{
    "deviceId" : "123423421", 
    "coords":{
      "latitude" : "39.3259013",
      "longitude": "-76.6305177"
    },
    "userInfo": {
        "userType": "student",
        "name": "Robin hood",
        "contact": "+1-980-775-6044",
        "emergency": "+1-904-777-0434",
        "age": 23,
        "gender":"M"
        },
    "destination": {
      "coords":{
              "latitude" : "39.3572681",
            "longitude": "-76.6242973"
          },
            "type": "campus"
          },
    "timestamp": "2016-02-06T00:13:19Z" ,
    "emergency": false,
    "enroute": true
  },{
    "deviceId" : "123423121", 
    "coords":{
      "latitude" : "39.3059013",
      "longitude": "-76.6005177"
    },
    "userInfo": {
        "userType": "security",
        "name": "Kaustub Sarkar",
        "contact": "+1-980-775-6044",
        "emergency": "+1-904-777-0434",
        "age": 33
        },
    "timestamp": "2016-02-06T00:23:19Z"       
  }]





new REST();
