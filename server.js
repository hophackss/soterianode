var express = require("express");
var rest = require("./rest.js");
var app  = express();
var config = require("./config.js");


var mongo = require('mongoskin');
//db = mongo.db("mongodb://localhost:27017/test", {native_parser:true});
db = mongo.db("mongodb://localhost:27017/test", function(err, db) {
  if(!err) {
    console.log("We are connected");
  } else {
    console.log("We are not connected");
  }
});


db.on('error', function(err){
    console.log('Error', err)
});

db.on('open', function () {
    console.log('Connection open')
});

db.on('disconnected', function(err){
    console.log('Desconected')
});



db.bind('myLocation',function(){
  console.log("bind myLocation");
  console.log(arguments);
  console.log(arguments[1]["s"]);
  console.log(arguments[1]["s"]["db"]);

//  db.close();

db.myLocation.findOne({"deviceId": ""},function(err, items) {
   console.log("tring to find using db.myLocation");
   console.log(items);
    db.close();

   });




});

// db.myLocation.find().toArray(function(err, items) {
//     console.log("Find error");    
//     console.log(err);
//     console.log(items);
//    // db.close();
// });





sample_json = [{
        "deviceId" : "123423423", 
        "coords":{
          "latitude" : "39.3299013",
          "longitude": "-76.6205177"          
        },
        "userInfo": {
            "userType": "student",
            "name": "Piyush Madan",
            "contact": "+1-980-777-6044",
            "emergency": "+1-980-777-0434",
            "age": 25,
            "gender":"M"
            },
        "desination": {
          "coords":{
                  "latitude" : "39.3372681",
                "longitude": "-76.6243973"
              },
                "type": "home"
              },
        "timestamp": "2016-02-06T00:20:19Z" ,
        "emergency": true,
        "enroute": true
      },{
        "deviceId" : "123423421", 
        "coords":{
          "latitude" : "39.3259013",
          "longitude": "-76.6305177"
        },
        "userInfo": {
            "userType": "student",
            "name": "Kaustub Sarkar",
            "contact": "+1-980-775-6044",
            "emergency": "+1-904-777-0434",
            "age": 23,
            "gender":"M"
            },
        "desination": {
          "coords":{
                  "latitude" : "39.3572681",
                "longitude": "-76.6242973"
              },
                "type": "campus"
              },
        "timestamp": "2016-02-06T00:13:19Z" ,
        "emergency": false,
        "enroute": true
      },{
        "deviceId" : "123423121", 
        "coords":{
          "latitude" : "39.3059013",
          "longitude": "-76.6005177"
        },
        "userInfo": {
            "userType": "security",
            "name": "Kaustub Sarkar",
            "contact": "+1-980-775-6044",
            "emergency": "+1-904-777-0434",
            "age": 33
            },
        "timestamp": "2016-02-06T00:23:19Z"       
      }]


db.myLocation.insert(sample_json,function(err) {
  console.log("try to db log");

    if(err) {
        return console.log('insert error', err);
    } else {
       console.log("no error");
    }

  });

db.open(function(err, db) {
  // Perform a simple insert into a collection
  var collection = db.collection("test");
  // Insert a simple doc
  collection.find({}, {}, function(err, result) {
    console.log(err);
    db.close();
  });
});



function REST(){
    var self = this;     
    // move this to mongodb connector once added
     self.configureExpress();
};


REST.prototype.configureExpress = function() {
      var self = this;
      var router = express.Router();

      app.param('univ', function(req, res, next, univ){
        req.collection = db.collection(univ)
        return next()
      })

      app.use('/', router);

      var rest_router = new rest(router);
      self.startServer();




// app.get('/collections/:collectionName', function(req, res, next) {
//   req.collection.find({} ,{limit:10, sort: [['_id',-1]]}).toArray(function(e, results){
//     if (e) return next(e)
//     res.send(results)
//   })
// })





}


REST.prototype.startServer = function() {
	console.log(config.port);
      app.listen(config.port,function(){
          console.log("All right ! I am alive at Port"+config.port);
      });
}

REST.prototype.stop = function(err) {
    console.log("ISSUE WITH SERVER \n" + err);
    process.exit(1);
}

new REST();